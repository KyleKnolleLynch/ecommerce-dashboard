# Ecommerce Dashboard

### 🔗 [Live admin dashboard](https://ecommerce-dashboard-beta.vercel.app)

- Nextjs with Typescript

- Planetscale with MySQL

- Prisma

- TailwindCSS

- Shadcn UI / Radix UI

- Clerk

- Cloudinary

- Tanstack/react table

- Stripe

- Zod

- React Hook Form

- Zustand

- Lucide React Icons

This admin dashboard also serves as a content managment system and API for multiple ecommerce websites/stores that can be created as needed. Signin/Signup authentication is set up with Clerk. You have the ability to create, update, and delete products, categories, sizes, colors, and billboard sections with background images and text. These, as well as product images, are easily uploaded and deleted using Cloudinary. API routes are also generated and available to make building C.R.U.D. functionality for different front-end websites easy.

Data is stored with PlanetScale MySQL using Prisma models, software I'm not very familiar with yet, so it was fun learning and using these great tools. Much of the styling and component building was done with shadcn/ui. This was the first time I've used shadcn and it was super nice to get the dashboard up and running fast and looking clean, I'm excited to use it more in the future. The rest of the styling was done with TailwindCSS.

Order history, status, and sales are shown in the admin panel, as well as a revenue graph built using Tanstack/react table.

The front-end storefront I built using this dashboard API can be seen here:

### 🔗 [Live storefront website](https://ecommerce-store-one-indol.vercel.app/)

### 🔗 [Source code for storefront website](https://gitlab.com/KyleKnolleLynch/ecommerce-store)

Credit and big thanks to [Antonio Erdeljac](https://www.youtube.com/watch?v=5miHyP6lExg&t=1s) for the great tutorial and codebase for this project.

This is a learning project for me, not something I claim to have built myself. But I wanted to document the experience. I completed and figured out as much as I could on my own as I worked through this tutorial. I enjoyed learning and practicing new tech, and tech I'm still building my skills with, like typescript, zod, shadcn/ui, zustand, and prisma in such a large and complex application.